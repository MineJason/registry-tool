﻿namespace MineJason.RegistryTool;

public record struct RegistryMapEntry
{
    public RegistryMapEntry(string targetPath, ResourceLocation identifier, RegistryInfo registryInfo, string className, string nameSpace)
    {
        TargetPath = targetPath;
        Identifier = identifier;
        RegistryInfo = registryInfo;
        ClassName = className;
        NameSpace = nameSpace;
    }

    public string TargetPath { get; set; }
    public ResourceLocation Identifier { get; set; }
    public RegistryInfo RegistryInfo { get; set; }
    public string ClassName { get; set; }
    public string NameSpace { get; set; }
}
