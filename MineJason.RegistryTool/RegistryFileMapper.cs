﻿namespace MineJason.RegistryTool;

using CaseExtensions;
using System;
using System.Collections.Generic;
using System.Text;

internal static class RegistryFileMapper
{
    internal static IEnumerable<RegistryMapEntry> MapRegistries(string baseDir, RegistryManifest manifest)
    {
        var result = new List<RegistryMapEntry>();
        foreach (var registry in manifest)
        {
            if (!ResourceLocation.TryParse(registry.Key, out var location))
            {
                Console.WriteLine("Invalid entry key {0}, ignoring", registry.Key);
            }

            var mapped = MapToPath(location, baseDir, out var className, out var nameSpace);

            result.Add(new RegistryMapEntry(mapped, location, registry.Value, className, nameSpace));
        }

        return result;
    }

    private static string MapToPath(ResourceLocation identifier, string baseDir, out string className, out string nameSpace)
    {
        var splitPath = identifier.Path.Split('/');
        var list = new List<string>(splitPath.Length + 1)
        {
            baseDir
        };
        var nameSpaceList = new List<string>();

        string? preClassName = null;

        foreach (var x in splitPath)
        {
            // Check if illegal path chars
            if (x == "." || x == "..")
            {
                continue;
            }

            var name = x.ToPascalCase();

            preClassName = name;
            list.Add(name);
            nameSpaceList.Add(name);
        }

        list[^1] = $"{list[^1]}.cs";
        nameSpaceList.RemoveAt(nameSpaceList.Count - 1);

        if (preClassName == null)
        {
            throw new ArgumentException("Invalid resource location: No applicable class name.", nameof(identifier));
        }

        className = preClassName;

        // Assemble the name space
        var nsBuilder = new StringBuilder();

        foreach (var x in nameSpaceList)
        {
            nsBuilder.Append('.');
            nsBuilder.Append(x);
        }

        nameSpace = nsBuilder.ToString();
        return Path.Combine([.. list]);
    }
}
