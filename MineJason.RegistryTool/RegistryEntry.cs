﻿namespace MineJason.RegistryTool;

public record struct RegistryEntry
{
    public int ProtocolId { get; set; }
}
