﻿namespace MineJason.RegistryTool;
using System.Text.Json.Serialization;

[JsonSerializable(typeof(RegistryEntry))]
[JsonSerializable(typeof(ResourceLocation))]
[JsonSerializable(typeof(RegistryEntryDictionary))]
[JsonSerializable(typeof(RegistryInfo))]
[JsonSerializable(typeof(RegistryManifest))]
[JsonSerializable(typeof(int))]
[JsonSourceGenerationOptions(PropertyNamingPolicy = JsonKnownNamingPolicy.SnakeCaseLower)]
internal partial class RegistryToolJsonContext : JsonSerializerContext
{
}
