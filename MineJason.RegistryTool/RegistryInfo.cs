﻿namespace MineJason.RegistryTool;
using System.Text.Json.Serialization;

public record RegistryInfo
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public ResourceLocation? Default { get; set; }

    public RegistryEntryDictionary Entries { get; set; } = [];

    public int ProtocolId { get; set; }
}
